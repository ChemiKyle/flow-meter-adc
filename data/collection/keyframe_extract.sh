# Extract keyframes from raspivid
cd ../images/


for f in *.h264; do
    name=$(echo "${f%.*}")
	  ffmpeg -i $f -vf "select=eq(pict_type\,I)" -vsync vfr vkf$name%04d.jpg
done

# Image Collection and Processing

A handful of scripts are provided for data collection. Weights provided were produced from a Raspberry PI Zero W v1.3 using the Raspberry Pi Camera v2 at 720px x 720px.


### Collection via Pictures
Initially, data was collected via stills. Collection can be started either using `press_pic.py` for button control or by using `tp.sh`. `press_pic.py` has toggles to use `PiCamera` or `raspistill`, the latter produces better quality than `press_picture.py`'s use of `PiCamera`.

### Collection via Video
Data collection was later moved to capturing videos and extracting the [keyframes](https://en.wikipedia.org/wiki/Key_frame#Video_compression) to label static images. Collection can be started either using `press_vid.py` for button control or `tv.sh`.

Extraction of keyframes is done by `extract_keyframes.sh`.


# Processing

I opted to use [LabelImg](https://github.com/tzutalin/labelImg) for its extreme simplicity of setup.  
A good alternative may be OpenCV's [CVAT](https://github.com/opencv/cvat) or UC Irvine's [VATIC](https://github.com/cvondrick/vatic) (if you're willing to set up Docker-CE containerization user accounts), they both allow annotation of video files directly. 

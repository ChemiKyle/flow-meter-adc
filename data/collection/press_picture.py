from RPi import GPIO as GPIO
import os
import time


def initialize_camera():
    cam = PiCamera()
    cam.resolution = (720, 720)
    return cam


def take_picture(cam):
    stamp = time.strftime("%Y%m%d-%H%M%S")
    cam.capture("/home/pi/projects/float_meter/training/capture/" + stamp + ".png")
    print("image captured")
    time.sleep(0.2)


def pic_by_pin(pin_num, cam, use_py_picam = True):
    GPIO.setmode(GPIO.BOARD)
    #GPIO.setup(pin_num, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(pin_num, GPIO.IN)
    while True:
        input_state = GPIO.input(pin_num)
        if not input_state:
            if use_py_picam:
                take_picture(cam)
            else:
                os.system("bash tp.sh")
        time.sleep(0.05)
        #GPIO.add_event_detect(pin_num, GPIO.RISING)
        #if GPIO.event_detected(pin_num):
        #    take_picture(cam)


def main():
    use_py_picam = False
    if use_py_picam:
        from picamera import PiCamera
        cam = initialize_camera()
        time.sleep(2)
        print('cam ready')
    else:
        cam = ""

    pic_by_pin(5, cam, use_py_picam)


if __name__ == "__main__":
    main()


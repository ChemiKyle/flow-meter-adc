from RPi import GPIO as GPIO
import os
import sys
import time

def vid_by_pin(pin_num):
    GPIO.setmode(GPIO.BOARD)
    #GPIO.setup(pin_num, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(pin_num, GPIO.IN)
    while True:
        input_state = GPIO.input(pin_num)
        if not input_state:
            #print("taking video")
            os.system("bash tv.sh")
            #print("video done")
        time.sleep(0.05)
        #GPIO.add_event_detect(pin_num, GPIO.RISING)
        #if GPIO.event_detected(pin_num):
        #    take_picture(cam)

def main():
    os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))
    time.sleep(2)
    print('cam ready')
    vid_by_pin(5) # using board pin 5 allows simply bridging 5 and 9 with a push button

if __name__ == "__main__":
    main()

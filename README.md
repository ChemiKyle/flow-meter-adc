# Introduction

This repository is an overview of training and implementing a neural network to read analog flow meters. At this stage it is a living document tracking the process from start to finish.  
Once complete, these instruction will remain but a simple implementation will be provided for anyone with the same end goal of digitizing the reading of an analog meter.

Instructions are provided in README files generated as I worked through the process; I tried to make them verbose as while the theory of machine learning and the YOLO algorithms may be readily accessible, information on the pipeline of actually generating and labelling your own data is surprisingly sparse.


# Creating Your Own
## Step 0: Data Collection
## Step 1: Data Annotation
See `data/collection` for scripts and a description.
